﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen.Data;
using Examen.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Examen.Controllers
{
    public class TransferController : Controller
    {
        private IStringLocalizer<MyResources> _myLocalizer { get; set; }
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public TransferController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IStringLocalizer<MyResources> myLocalizer)
        {
            _context = context;
            _userManager = userManager;
            _myLocalizer = myLocalizer;
        }
        // GET: Transfer
        public ActionResult Index()
        {
            return View();
        }

        // GET: Transfer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Transfer/Create
        public ActionResult Create()
        {
            string sum = _myLocalizer["Sum"];
            ViewData["Sum"] = sum;

            string cardId = _myLocalizer["CardId"];
            ViewData["CardId"] = cardId;

            string send = _myLocalizer["Send"];
            ViewData["Send"] = send;
            return View();
        }

        public ActionResult CreateAnonim(string id, double sum)
        {
            string TransferFault = _myLocalizer["TransferFault"];
            ViewData["TransferFault"] = TransferFault;

            string TransferSuccessful = _myLocalizer["TransferSuccessful"];
            ViewData["TransferSuccessful"] = TransferSuccessful;

            string resultMsg = string.Empty;
            
            var Exist = _userManager.Users.Any(x => x.UserName == id);  //получатель
            if (Exist)
            {
                var recipientUser = _context.User.FirstOrDefault(x => x.UserName == id);
                var recipientUserCard = _context.PersonalCard.FirstOrDefault(x => x.Id == recipientUser.PersonalCardId);
                recipientUserCard.Total = recipientUserCard.Total + sum;
                _context.Update(recipientUserCard);
                _context.SaveChanges();

                ActionTransfer action = new ActionTransfer()
                {
                    CreateData = DateTime.Now,
                    SenderId = null,
                    ReceiverId = recipientUser.Id,
                    SumAction = sum
                };

                _context.Add(action);
                _context.SaveChanges();
                return View("TransferSuccessful");
            }



            return View("Transferfault");

        }

        // POST: Transfer/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAction(string id, double sum)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Transfer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Transfer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Transfer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Transfer/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}