﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen.Data;
using Examen.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Examen.Controllers
{
    public class PersonalCardController : Controller
    {
        private IStringLocalizer<MyResources> _myLocalizer { get; set; }
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public PersonalCardController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IStringLocalizer<MyResources> myLocalizer)
        {
            _context = context;
            _userManager = userManager;
            _myLocalizer = myLocalizer;
        }
        // GET: PersonalCard
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Transfer()
        {
            string transfer = _myLocalizer["Transfer"];
            ViewData["Transfer"] = transfer;

            string sum = _myLocalizer["Sum"];
            ViewData["Sum"] = sum;

            string cardId = _myLocalizer["CardId"];
            ViewData["CardId"] = cardId;

            string send = _myLocalizer["Send"];
            ViewData["Send"] = send;

            return View();
        }

        public ActionResult TransferAccept(string id, int sum)
        {
            //var senderUser = _userManager.GetUserAsync(User);//отправитель

            string TransferFault = _myLocalizer["TransferFault"];
            ViewData["TransferFault"] = TransferFault;

            string TransferSuccessful = _myLocalizer["TransferSuccessful"];
            ViewData["TransferSuccessful"] = TransferSuccessful;

            string resultMsg = string.Empty;
            var senderUser = _context.User.Include(x => x.PersonalCard).FirstOrDefault(x => x.Id == _userManager.GetUserId(User));
            if (senderUser.PersonalCard.Total - sum < 0)
            {
                return View("TransferLowTotal");
            }
            var Exist = _userManager.Users.Any(x => x.UserName == id);  //получатель
            if (Exist)
            {
                var senderUserCard = _context.PersonalCard.FirstOrDefault(x => x.Id == senderUser.PersonalCardId);
                senderUserCard.Total = senderUserCard.Total - sum;
                _context.Update(senderUserCard);
                _context.SaveChanges();

                var recipientUser = _context.User.FirstOrDefault(x => x.UserName == id);
                var recipientUserCard = _context.PersonalCard.FirstOrDefault(x => x.Id == recipientUser.PersonalCardId);
                recipientUserCard.Total = recipientUserCard.Total + sum;
                _context.Update(recipientUserCard);
                _context.SaveChanges();

                ActionTransfer action = new ActionTransfer()
                {
                    CreateData = DateTime.Now,
                    SenderId = senderUser.Id,
                    ReceiverId = recipientUser.Id,
                    SumAction = sum
                };

                _context.Add(action);
                _context.SaveChanges();
                return View("TransferSuccessful");
            }
            
            return View("TransferFault");
        }

        // GET: PersonalCard/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PersonalCard/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PersonalCard/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PersonalCard/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PersonalCard/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: PersonalCard/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PersonalCard/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}