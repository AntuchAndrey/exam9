﻿using Examen.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Examen.Data
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        public string AvatarImage { get; set; }

        [ForeignKey("PersonalCard")]
        public int PersonalCardId { get; set; }
        public PersonalCard PersonalCard { get; set; }

        [ForeignKey("History")]
        public int HistoryId { get; set; }
        public History History { get; set; }
    }
}
