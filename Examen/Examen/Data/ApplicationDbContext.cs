﻿using System;
using System.Collections.Generic;
using System.Text;
using Examen.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Examen.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
      
        public DbSet<ApplicationUser> User { get; set; }
        public DbSet<PersonalCard> PersonalCard { get; set; }
        public DbSet<ActionTransfer> ActionTransfers { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        
    }
}
