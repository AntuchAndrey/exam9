﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace Examen
{
    public class CultureMiddleware
    {
        private readonly RequestDelegate _next;
        
        public CultureMiddleware(RequestDelegate next)
        {
            this._next = next;
        }
        
        public async Task Invoke(HttpContext context)
        {
            string[] acceptedLanguage = ((HttpRequestHeaders)context.Request.Headers).HeaderAcceptLanguage.ToArray();
            var lang = acceptedLanguage[0].Split(',')[0];

            if (!string.IsNullOrEmpty(lang))
            {
                try
                {
                    CultureInfo.DefaultThreadCurrentCulture = new CultureInfo(lang);
                    CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(lang);
                }
                catch (CultureNotFoundException) { }
            }
            await _next.Invoke(context);
        }

    }
    
    public static class CultureExtensions
    {
        public static IApplicationBuilder UseCulture(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CultureMiddleware>();
        }
    }
}
