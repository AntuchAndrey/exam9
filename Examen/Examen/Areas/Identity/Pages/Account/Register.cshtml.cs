﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Examen.Data;
using Examen.Models;
using Examen.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Examen.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _environment;
        private readonly FileUploadService _fileUploadService;
        private readonly ApplicationDbContext _context;

        public RegisterModel(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            IHostingEnvironment environment,
            FileUploadService fileUploadService,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _environment = environment;
            _fileUploadService = fileUploadService;
            _context = context;
        }

        public string GetNumber()
        {
            Random rnd = new Random();
            bool Exist = false;
            string num = String.Empty;
            while (!Exist)
            {
                int randNum = rnd.Next(1000000);
                num = randNum.ToString("D6");

                var users = _context.Users;
                var a = 0;
            }
            

            return num;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required]
            //[EmailAddress]
            //[Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 3)]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
            public string ConfirmPassword { get; set; }

            //[Required]
            public IFormFile AvatarImage { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");
            if (ModelState.IsValid)
            {
                PersonalCard personalAccount = new PersonalCard();
                _context.Add(personalAccount);
                _context.SaveChanges();

                History history = new History();
                _context.Add(history);
                _context.SaveChanges();

                var number = Convert.ToString(personalAccount.Id + 100000);

                var user = new ApplicationUser { HistoryId = history.Id, PersonalCardId = personalAccount.Id, UserName = number, Email = Input.Email, AvatarImage = Input.AvatarImage.FileName, };
                var path = Path.Combine(
                   _environment.WebRootPath,
                   $"images\\{user.UserName}\\avatar");
                _fileUploadService.Upload(path, Input.AvatarImage.FileName, Input.AvatarImage);
                user.AvatarImage = $"images/{user.UserName}/avatar/{Input.AvatarImage.FileName}";

                //string PersonalAccountNumber = GetNumber();

                
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {
                   

                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { userId = user.Id, code = code },
                        protocol: Request.Scheme);

                    await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                        $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return LocalRedirect(returnUrl);
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
