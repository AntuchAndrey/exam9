﻿using Examen.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Examen.Models
{
    public class ActionTransfer
    {
        public int Id { get; set; }
        public DateTime CreateData { get; set; }
        
        [ForeignKey("SenderApplicationUser")]
        public string SenderId { get; set; }
        public ApplicationUser SenderApplicationUser { get; set; }

        [ForeignKey("ReceiverApplicationUser")]
        public string ReceiverId { get; set; }
        public ApplicationUser ReceiverApplicationUser { get; set; }

        public double SumAction { get; set; }
    }
}
