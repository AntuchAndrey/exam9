﻿using Examen.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Examen.Models
{
    public class PersonalCard
    {
        public int Id { get; set; }

        //[ForeignKey("ApplicationUser")]
        //public string UserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        public double Total { get; set; }

        public PersonalCard()
        {
            Total = 1000.00;
        }
    }
}
